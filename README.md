**UPSTREAM: https://github.com/zzossig/hugo-theme-zzo**

This fork aims to eliminate unnecessary features and displays while maintaining the upstream style.

## Hugo Theme Zzo

Zzo is a blog theme for Hugo. It includes almost all features that a blog theme should have. Search, so on. It looks simple but once you deep dive into it, you can find out lots of functionalities.

## Table of contents

* [Screenshot](#screenshot)
* [Installation](#installation)
* [Updating](#updating)
* [Run example site](#run-example-site)
* [Configuration](#configuration)
* [Multi Language](#multi-language)
* [Customizing](#customizing)
* [External libraries](#external-library)

## Screenshot

![Hugo Theme Zzo Screenshot](images/screenshot.png)

## Installation

First of all, You need to add config files.
Follow the [Configuration](#configuration) step.

Then, You can download and unpack the theme manually from Github but it's easier to use git to clone the repo.

From the root of your site:

```bash
$ git clone https://gitlab.com/pastalian/hugo-theme-zzo.git themes/zzo
```

If you use git to version control your site, highly recommended, it's best to add the zzo theme as a submodule.

From the root of your site:

```bash
git submodule add https://gitlab.com/pastalian/hugo-theme-zzo.git themes/zzo
```

## Updating

From the root of your site:

```bash
git submodule update --remote --merge
```

## Run example site

From the root of themes/zzo/exampleSite:

```bash
hugo server --themesDir ../..
```

## Configuration

0. From the root of your site: delete config.toml file and add the files below

1. config folder structure. Keep in mind the underscore on the `_default` folder

```bash
root
├── config
│   ├── _default
│   │   ├── config.toml
│   │   ├── languages.toml
│   │   ├── menus.en.toml
│   │   ├── params.toml
```

2. config.toml

```bash
baseURL = "http://example.org/" # The URL of your site.
title = "Hugo Zzo Theme" # Title of your site
theme = "zzo" # Name of Zzo theme folder in `themes/`.

defaultContentLanguage = "en" # Default language to use (if you setup multilingual)
hasCJKLanguage = true # Set `true` for Chinese/Japanese/Korean languages.

summaryLength = 70 # The length of a post description on a list page.
buildFuture = false # if true, we can use future date

copyright = "©{year}, All Rights Reserved" # copyright symbol: $copy; current year: {year}
timeout = 10000
enableEmoji = true
paginate = 13 # Number of items per page in paginated lists.
rssLimit = 100

enableGitInfo = false # When true, the modified date will appear on a summary and single page. Since GitHub info needs to be fetched, this feature will slow down to build depending on a page number you have

[markup]
  [markup.goldmark]
    [markup.goldmark.renderer]
      hardWraps = true
      unsafe = true
      xHTML = true
  [markup.highlight]
    codeFences = true
    lineNos = true
    lineNumbersInTable = true
    noClasses = false
  [markup.tableOfContents]
    endLevel = 3
    ordered = false
    startLevel = 2

[outputs]
  home = ["HTML", "RSS", "JSON"]

[taxonomies]
  category = "categories"
  tag = "tags"
```

3. languages.toml

```bash
[en]
  title = "Hugo Zzo Theme"
  languageName = "English"
  weight = 1

[ko]
  title = "Hugo Zzo Theme"
  languageName = "한국어"
  weight = 2
```

4. menus.en.toml

You shoud make your own menu.

```bash
[[main]]
  identifier = "about"
  name = "about"
  url = "about"
  weight = 1

[[main]]
  identifier = "archive"
  name = "archive"
  url = "archive"
  weight = 2

[[main]]
  identifier = "posts"
  name = "posts"
  url = "posts"
  weight = 3
    
[[main]]
  identifier = "notes"
  name = "notes"
  url = "notes"
  weight = 4
...
```

5. params.toml

```bash
logoText = "Zzo" # Logo text that appears in the site navigation bar.
logoType = "short" # long, short -> short: squre shape includes logo text, long: rectangle shape not includes logo text
logo = true # Logo that appears in the site navigation bar.
description = "The Zzo theme for Hugo example site." # for SEO
custom_css = [] # custom_css = ["scss/custom.scss"] and then make file at root/assets/scss/custom.scss
custom_js = [] # custom_js = ["js/custom.js"] and then make file at root/assets/js/custom.js

colorScheme = "dark" # ["dark", "light", "hacker", "solarized", "kimbie"]
notAllowedTypesInHomeSidebar = ["about", "archive"] # not allowed page types in home page sidebar(recent post titles).
notAllowedTypesInArchive = ["about"] # not allowed page types in archive page

viewportSize = "normal" # widest, wider, wide, normal, narrow
hideSingleContentsWhenJSDisabled = false
minItemsToShowInTagCloud = 1 # Minimum items to show in tag cloud

# menu
showMobileMenuTerms = ["tags", "categories"]

# body
enableSearch = true # site search with Fuse
enableSearchHighlight = true # when true, search keyword will be highlighted
summaryShape = "classic" # card, classic, compact
archiveGroupByDate = "2006" # "2006-01": group by month, "2006": group by year
archivePaginate = 13 # items per page
paginateWindow = 1 # setting it to 1 gives 7 buttons, 2 gives 9, etc. If set 1: [1 ... 4 5 6 ... 356] [1 2 3 4 5 ... 356] etc

# sidebar
enableHomeSidebarTitles = true
enableListSidebarTitles = true
enableToc = true # single page table of contents, you can replace this param to toc(toc = true)
hideToc = false # Hide or Show toc
tocFolding = false
enableTocSwitch = true # single page table of contents visibility switch
itemsPerCategory = 5 # maximum number of posts shown in the sidebar.
tocLevels = ["h2", "h3", "h4"] # minimum h2, maximum h4 in your article

# footer
showPoweredBy = true # show footer text: Powered by Hugo and Zzo theme

[copyrightOptions]
  enableCopyrightLink = false # if set, you can add copyright link
  copyrightLink = ""
  copyrightLinkImage = ""
  copyrightLinkText = ""
```

## Multi Language

The default language of this theme is English. If you want to use another language, follow these steps

1. Make a menu file.

```bash 
root
├── config
│   ├── _default
│   │   ├── ...
│   │   ├── menus.ko.toml
```

```bash
config/_default/menus.ko.toml

[[main]]
  identifier = "about"
  name = "about"
  url = "/about/"
  weight = 1

[[main]]
    identifier = "archive"
    name = "archive"
    url = "/archive/"
    weight = 2
...
```

2. Make a content file. Add your language code before the md extension.

```bash
hugo new about/index.ko.md
hugo new posts/markdown-syntax.ko.md
...
```

3. Make an i18n file.

```bash
i18n/ko.toml

[search-placeholder]
other = "검색..."

[summary-dateformat]
other = "2006년 01월 02일"

[tags]
other = "태그"

...
```

4. Edit config.toml file.

```bash
defaultContentLanguage = "ko"
hasCJKLanguage = true
```

## Customizing

It's a better idea not to modify the Zzo theme's folder if you want future support and upgrade. (You can modify if it doesn't matter) If you want more customizing options, open a new issue.

### custom css

1. Add this line of code to your params.toml file.

```bash
config/_default/params.toml

...
custom_css = ["css/custom.css", "scss/custom.scss", ...]
...
```

2. Add your file to assets folder. Filename must match with config params you set above.

```bash
assets/scss/custom.scss
assets/css/custom.css
```

3. If you want to modify the Zzo theme's default color, you should override the theme style. For example, if you're going to change the body background-color because I set the background-color in #body selector, not in the body tag selector, you should override body background-color there. Body tag selector won't work. And make sure to set !important. After setting the values, restart Hugo.

```css
assets/scss/custom.scss or assets/css/custom.css

...
#body {
  background-color: red !important;
}
...
```

### custom js

1. Add this line of code to your params.toml file.

```bash
config/_default/params.toml

...
custom_js = ["js/custom.js", ...]
...
```

2. Add your file to assets folder. Filename must match with config params you set above.

```bash
assets/js/custom.js
```

### custom syntax highlighting

1. Make a skin.toml file at root/data folder. Set the theme_dark_chroma, theme_light_chroma, ... params value as you want. Refer this [link](https://xyproto.github.io/splash/docs/all.html). If theme_[xxxx]_chroma value include - or _ like special character, just delete it.
For example, if you want use solarized-dark256 style, set the param like this.

```
root/data/skin.toml

theme_dark_chroma = "solarizeddark256"
```

2. Add a custom style file if you want to change specific colors. [[custom-css](#custom-css)]
Then, override chroma class. You can find this class at themes/zzo/assets/sass/syntax folder.
Example code is like this.

```
root/assets/scss/custom.scss

.chroma {
  background-color: #123456 !important;
}
```

Make sure that !important is necessary. After you changed this param, restart hugo.

### custom grid

1. Make a grid.toml file in data folder. (data/grid.toml)

2. Copy the contents of themes/zzo/data/grid.toml file and paste it into the grid.toml file you created above.

3. Change the grid as you want.

4. Once you change the grid.toml file, restart hugo.

```toml
data/grid.toml example

grid_max_width = "960"
grid_max_unit = "px" #  "px", "\"%\""  Using% is limited to using full width.
grid_main_main_width = "5"
grid_main_main_unit = "fr" # "fr", "px"
grid_main_side_width = "2"
grid_main_side_unit = "fr" # "fr", "px"
grid_column_gap_width = "32"
grid_column_gap_unit = "px" # "px"
grid_navbar_height = "50px" # "px"
grid_row_gap = "0"
```

### custom font

1. Add custom css file

```bash
config/_default/params.toml

...
custom_css = ["css/font.css"]
...
```

Set the above param and add file to assets/css/font.css

2. In your font.css file, add font-face something like this.

```css
@font-face {
    font-family: 'Montserrat';
    src: url('../fonts/montserrat-black.woff2') format('woff2'),
         url('../fonts/montserrat-black.woff') format('woff');
    font-weight: 900;
    font-style: normal;
}

@font-face {
    font-family: 'Merriweather';
    src: url('../fonts/merriweather-regular.woff2') format('woff2'),
         url('../fonts/merriweather-regular.woff') format('woff');
    font-weight: 400;
    font-style: normal;
}
```

3. Add your fonts file at root/static/fonts/myfont.xxx (If your url in step2 is ('../fonts/myfont.xxx')).

4. Make a font.toml file at root/data/font.toml and make variables as below.

```toml
title_font = "\"Montserrat\", sans-serif"
content_font = "\"Merriweather\", serif"
```

5. Another approach

Make a file at root/layouts/partials/head/custom-head.html and then load font style. 

```html
<link href="https://fonts.googleapis.com/css?family=Noto+Sans+KR:400,700&display=swap&subset=korean" rel="stylesheet">
```

### custom copyright

If you want to add a link to the footer copyright, not just a text, you can customize it.

1. In your config.toml file, set the copyright param like this.
```toml
...
copyright = This is my {} copyright text
...
```
The {} part will be your copyright link.
2. In your params.toml file, set the copyrightOptions params

```toml
...
[copyrightOptions]
  enableCopyrightLink = false
  copyrightLink = "https://..."
  copyrightLinkImage = "https://..."
  copyrightLinkText = "copyright link text"
```

### custom favicon

Override the default favicon by adding your favicon to the /static directory. `favicon.svg` is the only file this theme uses by default.

## External Library

If you want use external libraries, this theme currently supporting Katex, MathJax, Just add some variable to a front-matter.

```bash
---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
...
libraries:
- katex 
- mathjax
---

```
